-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2023 at 04:05 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hidroponik`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT current_timestamp(),
  `MESINID` int(11) NOT NULL,
  `DHTTEMP` double NOT NULL,
  `DHTHUM` double NOT NULL,
  `PH` double NOT NULL,
  `TDS` double NOT NULL,
  `EC` double NOT NULL,
  `DALLASTEMP` double NOT NULL,
  `SONAR1` double NOT NULL,
  `SONAR2` double NOT NULL,
  `EMG` tinyint(1) NOT NULL,
  `FLOATSWITCH` tinyint(1) NOT NULL,
  `LEVEL1` double NOT NULL,
  `LEVEL2` double NOT NULL,
  `LEVEL3` double NOT NULL,
  `LEVEL4` double NOT NULL,
  `LEVEL5` double NOT NULL,
  `FLOWMETER` double NOT NULL,
  `AMPEREMETER` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `timestamp`, `MESINID`, `DHTTEMP`, `DHTHUM`, `PH`, `TDS`, `EC`, `DALLASTEMP`, `SONAR1`, `SONAR2`, `EMG`, `FLOATSWITCH`, `LEVEL1`, `LEVEL2`, `LEVEL3`, `LEVEL4`, `LEVEL5`, `FLOWMETER`, `AMPEREMETER`) VALUES
(1, '2023-01-03 15:52:43', 0, 0, 0, 0, 84.21053, -0.180537, 0, 0, 0, 1, 1, 7, 7, 7, 7, 7, 23, 1),
(2, '2023-01-03 15:55:49', 0, 0, 0, 0, 84.21053, -0.180537, 0, 0, 0, 1, 1, 7, 7, 7, 7, 7, 23, 1),
(3, '2023-01-03 15:58:14', 0, 0, 0, 0, 84.21053, -0.180537, 0, 0, 0, 1, 1, 7, 7, 7, 7, 7, 23, 1),
(4, '2023-01-03 16:04:37', 0, 0, 0, 0, 84.21053, -0.180537, 0, 0, 0, 1, 1, 7, 7, 7, 7, 7, 23, 1),
(5, '2023-01-04 03:04:21', 1, 0, 0, 0, 84.21053, -0.180537, 0, 0, 0, 1, 1, 7, 7, 7, 7, 7, 23, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
