<?php

    //Make sure that it is a POST request.
    if(strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') != 0){
        throw new Exception('Request method must be POST!');
    }

    //Make sure that the content type of the POST request has been set to application/json
    $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
    if(strcasecmp($contentType, 'application/json') != 0){
        throw new Exception('Content type must be: application/json');
    }

    //Receive the RAW post data.
    $content = trim(file_get_contents("php://input"));

    //Attempt to decode the incoming RAW post data from JSON.
    $data = json_decode($content, true);

    //If json_decode failed, the JSON is invalid.
    if(!is_array($data)){
        throw new Exception('Received content contained invalid JSON!');
    }

    //connect to database
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "hidroponik";
    $DHTTEMP = $data["DHTTEMP"];
    $DHTHUM = $data["DHTHUM"];
    $PH = $data["PH"];
    $TDS = $data["TDS"];
    $EC = $data["EC"];
    $DALLASTEMP = $data["DALLASTEMP"];
    $SONAR1 = $data["SONAR1"];
    $SONAR2 = $data["SONAR2"];
    $EMG = $data["EMG"];
    $FLOATSWITCH = $data["FLOATSWITCH"];
    $LEVEL1 = $data["LEVEL1"];
    $LEVEL2 = $data["LEVEL2"];
    $LEVEL3 = $data["LEVEL3"];
    $LEVEL4 = $data["LEVEL4"];
    $LEVEL5 = $data["LEVEL5"];
    $FLOWMETER = $data["FLOWMETER"];
    $AMPEREMETER = $data["AMPEREMETER"];
    try 
    {
        
        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO data (DHTTEMP, DHTHUM, PH, TDS, EC, DALLASTEMP, SONAR1, SONAR2, EMG, FLOATSWITCH, LEVEL1, LEVEL2, LEVEL3, LEVEL4, LEVEL5, FLOWMETER, AMPEREMETER)
        VALUES ($DHTTEMP, $DHTHUM, $PH, $TDS, $EC, $DALLASTEMP, $SONAR1, $SONAR2, $EMG, $FLOATSWITCH, $LEVEL1, $LEVEL2, $LEVEL3, $LEVEL4, $LEVEL5, $FLOWMETER, $AMPEREMETER)";
        // use exec() because no results are returned
        $conn->exec($sql);
        echo "New record created successfully";
    } catch(PDOException $e) {
        echo $sql . "<br>" . $e->getMessage();
    }
    
        $conn = null;
?>