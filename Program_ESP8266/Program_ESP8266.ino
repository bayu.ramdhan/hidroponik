#include <NTPClient.h>
// change next line to use with another board/shield
#include <ESP8266WiFi.h>
//#include <WiFi.h> // for WiFi shield
//#include <WiFi101.h> // for WiFi 101 shield or MKR1000
#include <WiFiUdp.h>
#include <ArduinoJson.h>

const char *ssid     = "MSC Startups";
const char *password = "meadowstreetcapital2";
//char ssid[] = "MSC Startups";
//char pass[] = "meadowstreetcapital2";
const String url_server  = "";
unsigned long currentMillis = 0;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);

#include <ESP8266HTTPClient.h>
// #define SERVER_IP "192.168.100.211:8080"
#define SERVER_IP "192.168.1.19:80"

void setup(){
  Serial.setRxBufferSize(256);
  // Serial.setTxBufferSize(256);
  Serial.begin(115200);

  WiFi.begin(ssid, password);

  while ( WiFi.status() != WL_CONNECTED ) {
    delay ( 500 );
    Serial.print ( "." );
  }
  Serial.println("");
  Serial.print("Connected! IP address: ");
  Serial.println(WiFi.localIP());

  timeClient.begin();
}
String inString;
StaticJsonDocument<500> docAll;
void loop() {
  timeClient.update();

//  Serial.println(timeClient.getFormattedTime());
  delay(1000);
  StaticJsonDocument<200> doc;

  while (Serial.available()) {
    // Чтение данных из порта Serial3
    char inChar = Serial.read();
    // Вывод прочитанных данных в порт Serial
    Serial.write(inChar);
    // Поиск команды в полученных данных (команда должна быть в квадратных скобках)
    inString += inChar;
    if (inChar == '}') {
      DeserializationError error = deserializeJson(doc, inString);
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.print(error.f_str());
        Serial.println("]");
      }
      else
      {
        Serial.print(F("Parsing sukses!"));
        Serial.println();
        if(doc["data"]==1)
        {
          docAll["MESINID"] = doc["MESINID"];
          docAll["DHTTEMP"] = doc["DHTTEMP"];
          docAll["DHTHUM"] = doc["DHTHUM"];
          docAll["PH"] = doc["PH"];
          docAll["TDS"] = doc["TDS"];
          docAll["EC"] = doc["EC"];
          docAll["DALLASTEMP"] = doc["DALLASTEMP"];
          // Serial.print(String(docAll["DALLASTEMP"]));
          serializeJsonPretty(docAll, Serial);

        }
        else if(doc["data"]==2)
        {
          docAll["SONAR1"] = doc["SONAR1"];
          docAll["SONAR2"] = doc["SONAR2"];
          docAll["EMG"] = doc["EMG"];
          docAll["FLOATSWITCH"] = doc["FLOATSWITCH"];
          docAll["LEVEL1"] = doc["LEVEL1"];
          docAll["LEVEL2"] = doc["LEVEL2"];
          docAll["LEVEL3"] = doc["LEVEL3"];
          // Serial.print(String(docAll["LEVEL3"]));
          serializeJsonPretty(docAll, Serial);
        }
        else
        {
          docAll["LEVEL4"] = doc["LEVEL4"];
          docAll["LEVEL5"] = doc["LEVEL5"];
          docAll["FLOWMETER"] = doc["FLOWMETER"];
          docAll["AMPEREMETER"] = doc["AMPEREMETER"];
//          Serial.print(String(docAll["AMPEREMETER"]));
          serializeJsonPretty(docAll, Serial);
          Serial.print("]");

          WiFiClient client;
          HTTPClient http;

          Serial.print("HTTP begin...\n");
          // configure traged server and url
          http.begin(client, "http://" SERVER_IP "/hidroponikAPI/insert.php"); //HTTP
          http.addHeader("Content-Type", "application/json");

          Serial.print("HTTP POST...\n");
          // start connection and send HTTP header and body
          String json = "";
          serializeJson(docAll, json);
          int httpCode = http.POST(json);
          delay(100);
          if (httpCode == HTTP_CODE_OK) {
            const String& payload = http.getString();
            if (payload=="New record created successfully") {
              Serial.println(payload);
            } 
            else
            {
              Serial.println("Pengiriman error");
            }
          }
          else
          {
            Serial.println(httpCode);
          }
          
          http.end();
          // docAll.clear();
        }
        Serial.print("]");
        // serializeJson(docAll, Serial);
      }
      inString = "";
    }
  }
  int hour =timeClient.getHours();
  if(millis()-currentMillis > 60000)
  {
    currentMillis = millis();
    if((hour>=0&&hour<3)||(hour>=8&&hour<10))
    {
      Serial.println("[ON]");
    }
    else
    {
      Serial.println("[OFF]");
    }
  }

}

