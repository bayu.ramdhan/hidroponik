void ProgManual()
{
  lcd.setCursor(14,0);                   // atur baris LCD baris 1 kolom ke 6
  lcd.print("MANUAL");
  bool PHUp = getButton(pin_PBPHUp);
  bool PHDown = getButton(pin_PBPHDown);
  bool NutrisiA = getButton(pin_PBNutrisiA);
  bool NutrisiB = getButton(pin_PBNutrisiB);
  
  setVal(pin_PHUp, PHUp);
  setVal(pin_PHDown, PHDown);
  setVal(pin_NutrisiA, NutrisiA);
  setVal(pin_NutrisiB, NutrisiB);
  
  setVal(pin_LEDHijau, LOW);
  setVal(pin_LEDMerah, LOW);
  setVal(pin_LampuTaman, LOW);
  setVal(pin_PompaAirAON, LOW);
  setVal(pin_PompaAirBaku, LOW);
//  setVal(pin_PompaAirLimit, LOW);
  setVal(pin_AC, LOW);

  LCDManual(PHUp, PHDown, NutrisiA, NutrisiB);
}
