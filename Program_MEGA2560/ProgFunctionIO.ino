bool getButton(int pin)
{
  bool val = digitalRead(pin);
  bool valInverse = 1 - val;
  return valInverse;
}
void setVal(int pin, bool val)
{
  bool stat = 1 - val;
  digitalWrite(pin, stat);
}


void setupPin()
{
  pinMode(pin_PBPHUp, INPUT_PULLUP);
  pinMode(pin_PBPHDown, INPUT_PULLUP);
  pinMode(pin_PBNutrisiA, INPUT_PULLUP);
  pinMode(pin_PBNutrisiB, INPUT_PULLUP);
  pinMode(pin_FloatSwitch, INPUT_PULLUP);
  pinMode(pin_AUTO, INPUT_PULLUP);
  pinMode(pin_NTP, INPUT_PULLUP);

  pinMode(pin_LEDHijau, OUTPUT);
  pinMode(pin_LEDMerah, OUTPUT);
//  pinMode(pin_Buzzer, OUTPUT);
  pinMode(pin_LampuTaman, OUTPUT);
  pinMode(pin_PompaAirAON, OUTPUT);
  pinMode(pin_PompaAirLimit, OUTPUT);
  pinMode(pin_PHUp, OUTPUT);
  pinMode(pin_PHDown, OUTPUT);
  pinMode(pin_NutrisiA, OUTPUT);
  pinMode(pin_NutrisiB, OUTPUT);
  pinMode(pin_AC, OUTPUT);
  pinMode(pin_PompaAirBaku, OUTPUT);

//modifikasi sonar
  pinMode(48, OUTPUT);
  digitalWrite(48, HIGH);
  pinMode(50, OUTPUT);
  digitalWrite(50, HIGH);
}

int tdsArray[40];   //Store the average value of the sensor feedback
int tdsArrayIndex=0;
static unsigned long tdssamplingTime = millis();
float getTDS(float suhu)
{
  gravityTds.setTemperature(suhu);  // set the temperature and execute temperature compensation
  gravityTds.update();  //sample and calculate
  float val = gravityTds.getTdsValue();  // then get the value
  
  if(millis()-tdssamplingTime > 20)
  {  
    tdsArray[tdsArrayIndex++]=val;
    if(tdsArrayIndex==40)tdsArrayIndex=0;
    val = avergearray(tdsArray, 40);
  }
  return val;
}
void getTDS2(float &tdsvalue, float &ecvalue)
//float getTDS2()
{
  int val2 = analogRead(pin_TDS);
  float teg = val2 * (5.0/1023);

//  float val = 2000/5*teg;
  if(millis()-tdssamplingTime > 20)
    {  
      tdsArray[tdsArrayIndex++]=teg;
      if(tdsArrayIndex==40)tdsArrayIndex=0;
      teg = avergearray(tdsArray, 40);
    }
  ecvalue = (0.3442 * teg) - 0.253;
  tdsvalue = 2000/5*teg;
//  tdsvalue = (211.2254 * teg) - 144.1466;
//  return val;
}
float getDallas(float &inVal)
{
  sensors.requestTemperatures();
  float val = sensors.getTempCByIndex(0);  // set the temperature and execute temperature compensation
  if (val != DEVICE_DISCONNECTED_C)
  {
    return val;
  }
  else
  {
    return inVal;
  }

}


float calibration_value = 21.34 - 0.7;
int phval = 0;
unsigned long int avgval;
int buffer_arr[10], temp;
float getPH()
{
  for (int i = 0; i < 10; i++)
  {
    buffer_arr[i] = analogRead(pin_PH);
    delay(30);
  }
  for (int i = 0; i < 9; i++)
  {
    for (int j = i + 1; j < 10; j++)
    {
      if (buffer_arr[i] > buffer_arr[j])
      {
        temp = buffer_arr[i];
        buffer_arr[i] = buffer_arr[j];
        buffer_arr[j] = temp;
      }
    }
  }
  avgval = 0;
  for (int i = 2; i < 8; i++)
    avgval += buffer_arr[i];
  float volt = (float)avgval * 5.0 / 1024 / 6;
  float ph_act = -5.70 * volt + calibration_value;
  return ph_act;
}


#define Offset 0.00            //deviation compensate
#define samplingInterval 20 
#define printInterval 50 //500
#define ArrayLenth  40    //times of collection
int pHArray[ArrayLenth];   //Store the average value of the sensor feedback
int pHArrayIndex=0;
static unsigned long samplingTime = millis();
float getPH2()
{

  if(millis()-samplingTime > samplingInterval)
  {
      pHArray[pHArrayIndex++]=analogRead(pin_PH);
      if(pHArrayIndex==ArrayLenth)pHArrayIndex=0;
      float voltage1 = avergearray(pHArray, ArrayLenth)*5.0/1024;
      float pHValue = 3.6*voltage1+Offset; //3.7 
      samplingTime=millis();
      return pHValue;
  }
}
void getTemperature(float &temperature, float &humidity)
{
  int err = SimpleDHTErrSuccess;
  float temp, hum;
  if ((err = dht22.read2(&temp, &hum, NULL)) == SimpleDHTErrSuccess) {
    temperature = temp;
    humidity = hum;
  }
}
float getSonar1()
{
  delay(30);
  return ultrasonic1.ping_cm();
}

float getSonar2()
{
  delay(30);
  return ultrasonic2.ping_cm();
}



float getLevel(int pin)
{
  float sensorValue = analogRead(pin);
  float val = map(sensorValue, 0, 1023, levelMin, levelMax);
  return val;
}

unsigned long current_setAlarm = millis();
bool buzzerStat = false;
void setAlarm(bool stat)
{
  if (stat == true)
  {
    setVal(pin_LEDHijau, LOW);
    setVal(pin_LEDMerah, HIGH);
  }
  else
  {
    setVal(pin_LEDHijau, HIGH);
    setVal(pin_LEDMerah, LOW);
  }
}

float getKnob(int pin, float minVal, float maxVal)
{
  float sensorValue = analogRead(pin);
  float val = map(sensorValue, 0, 1023, minVal, maxVal);
  return val;
}
float getKnobPH(int pin, float minVal, float maxVal)
{
  float sensorValue = analogRead(pin);
  float val = sensorValue * (14.0/1023);
  return val;
}


double avergearray(int* arr, int number){
  int i;
  int max,min;
  double avg;
  long amount=0;
  if(number<=0){
    //Serial.println("Error number for the array to avraging!/n");
    return 0;
  }
  if(number<5){   //less than 5, calculated directly statistics
    for(i=0;i<number;i++){
      amount+=arr[i];
    }
    avg = amount/number;
    return avg;
  }else{
    if(arr[0]<arr[1]){
      min = arr[0];max=arr[1];
    }
    else{
      min=arr[1];max=arr[0];
    }
    for(i=2;i<number;i++){
      if(arr[i]<min){
        amount+=min;        //arr<min
        min=arr[i];
      }else {
        if(arr[i]>max){
          amount+=max;    //arr>max
          max=arr[i];
        }else{
          amount+=arr[i]; //min<=arr<=max
        }
      }//if
    }//for
    avg = (double)amount/(number-2);
  }//if
  return avg;
}

int X;
int Y;
float TIME = 0;
float FREQUENCY = 0;
float WATER = 0;
float TOTAL = 0;
float LS = 0;
float getFlow(int input)
{
  X = pulseIn(input, HIGH);
  Y = pulseIn(input, LOW);
  TIME = X + Y;
  FREQUENCY = 1000000/TIME;
  WATER = FREQUENCY/7.5;
  LS = WATER/60;
  float val = 0;
  if(FREQUENCY >= 0)
  {
    if(isinf(FREQUENCY))
    {
      val = 0;
    }
    else
    {
      TOTAL = TOTAL + LS;
      val = WATER;
    }
  }
  else
  {
    val = 0;
  }
}
