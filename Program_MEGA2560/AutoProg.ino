
bool airBakuTDS, airBakuLevel;
void ProgAuto(float phVal, float tdsVal, float temp, float phSet, float tdsSet, int jedaSet, int waktuSet, bool EMGstatus)
{
  lcd.setCursor(14,0);                   // atur baris LCD baris 1 kolom ke 6
  lcd.print(" AUTO ");
  //cek kondisi ph dan mengondisikan ph
  AutoPH(phVal, phSet, jedaSet, waktuSet);

  //cek kondisi TDS dan mengondisikan TDS
  AutoTDS(tdsVal, tdsSet, jedaSet, waktuSet);


  //kondisi menyalakan pompa air cadangan ketika bak1 limit
//  float bak1val = getSonar1();
//  if(bak1val<=bak1Min)

  //output kondisi alarm
  if(EMGstatus==true)
  {
    setVal(pin_PompaAirAON, HIGH);
    airBakuLevel = 0;
    setAlarm(false);
  }
  else
  {
    setVal(pin_PompaAirAON, LOW);
    airBakuLevel = 1;
    setAlarm(true);
  }
  if((airBakuLevel==true)||(airBakuTDS==true))
  {
    setVal(pin_PompaAirBaku, HIGH);
  }
  else
  {
    setVal(pin_PompaAirBaku, LOW);
  }
  if(temp>=DHTMax)
  {
    setVal(pin_AC, HIGH);
  }
  if(temp<=DHTMin)
  {
    setVal(pin_AC, LOW);
  }

}
