
/***************************************
 * Hermawan Isbandi
 * Smart Hydrophonic System 
 * Board  : NodeMCU  V3
 * Input  : Sensor Soil Moisture, DHT11, DS18B20
 * Output : Relay, Blynk Android & LCD 16x2
 * IoT Smart Hydrophonic
 * Meadow Street Capital
 * 14 October 2022
 * hermawan.isbandi@gmail.com
 ****************************************/
#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal_I2C.h>
#include <SimpleDHT.h>
#include <NewPing.h>
#include "GravityTDS.h"
#include <ArduinoJson.h>

#define MAX_DISTANCE 400

#include <Wire.h> 
#include <Robojax_WCS.h>
#define MODEL 11 //see list above

#define ZERO_CURRENT_WAIT_TIME 5000 //wait for 5 seconds to allow zero current measurement
#define CORRECTION_VLALUE 164 //mA
#define MEASUREMENT_ITERATION 100
#define VOLTAGE_REFERENCE  5000.0 //5000mv is for 5V
#define BIT_RESOLUTION 10
#define DEBUT_ONCE true

// #define SERIAL_TX_BUFFER_SIZE 256
// #define SERIAL_RX_BUFFER_SIZE 256
