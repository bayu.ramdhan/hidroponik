#include "config.h"
#include "library.h"

NewPing ultrasonic1 = NewPing(pin_bak1Trig, pin_bak1Echo, MAX_DISTANCE);
NewPing ultrasonic2 = NewPing(pin_bak2Trig, pin_bak2Echo, MAX_DISTANCE);

SimpleDHT22 dht22(pin_DHT22);

OneWire oneWire(pin_DallasTemp); 
GravityTDS gravityTds;
 
DallasTemperature sensors(&oneWire);
LiquidCrystal_I2C lcd(0x27, 20, 4);

Robojax_WCS wcssensor(
          MODEL, pin_Ampere, SENSOR_VCC_PIN, 
          ZERO_CURRENT_WAIT_TIME, ZERO_CURRENT_LED_PIN,
          CORRECTION_VLALUE, MEASUREMENT_ITERATION, VOLTAGE_REFERENCE,
          BIT_RESOLUTION, DEBUT_ONCE           
          );
          
void setup() {
  // put your setup code here, to run once:
  setupPin();
  gravityTds.setPin(pin_TDS);
  gravityTds.setAref(5.0);  //reference voltage on ADC, default 5.0V on Arduino UNO
  gravityTds.setAdcRange(1024);  //1024 for 10bit ADC;4096 for 12bit ADC
  gravityTds.begin();  //initialization
  sensors.begin();
  lcd.begin();                      // initialize the lcd 
  lcd.backlight();
  Serial.begin(115200);
  Serial3.begin(115200);
  
  wcssensor.start();
  Serial.println("Ready!..");

}
float DALLAS, TEMP, HUM, PH, EC, TDS, SONAR1, SONAR2, FLOWMETER, AMPEREMETER;
float level1, level2, level3, level4, level5;
bool alarmStatus = false;

bool setValueAuto, man_auto, FLOATSWITCH;
float setValPHAuto, SetValTDSAuto;
int setvalJedaAuto, setValWaktuAuto;
void loop() {
  man_auto = getButton(pin_AUTO);
  FLOATSWITCH = getButton(pin_FloatSwitch);
  PH= getPH2();
  DALLAS = getDallas(DALLAS);
  getTDS2(TDS, EC);
  getTemperature(TEMP, HUM);
  SONAR1 = getSonar1();
  SONAR2 = getSonar2();


  SetValPH = getKnobPH(pin_KnobPH, 0, 14);
  SetValTDS = getKnob(pin_KnobTDS, 0, 2000);
  SetValJeda = getKnob(pin_KnobJeda, 1, 10);
  SetValWaktu = getKnob(pin_KnobWaktu, 1, 10);

  level1 = getLevel(pin_WaterLevel1);
  level2 = getLevel(pin_WaterLevel2);
  level3 = getLevel(pin_WaterLevel3);
  level4 = getLevel(pin_WaterLevel4);
  level5 = getLevel(pin_WaterLevel5);

  wcssensor.readCurrent();//this must be inside loop
  AMPEREMETER = wcssensor.getCurrent();
  FLOWMETER = getFlow(pin_FlowMeter);
  if(man_auto == true)
  {
    if((level1>=levelAlarm)||(level2>=levelAlarm)||(level3>=levelAlarm)||(level4>=levelAlarm)||(level5>=levelAlarm)||(FLOATSWITCH==true))
    {
      alarmStatus = true;
    }
    else
    {
      alarmStatus = false;
    }
    if(setValueAuto==false)
    {
      setValPHAuto = SetValPH;
      SetValTDSAuto = SetValTDS;
      setvalJedaAuto = SetValJeda;
      setValWaktuAuto = SetValWaktu;
      setValueAuto = true;
    }
//    ProgAuto(PH, EC, TEMP, SetValPH, SetValTDS, SetValJeda, SetValWaktu, alarmStatus);
    ProgAuto(PH, TDS, TEMP, setValPHAuto, SetValTDSAuto, setvalJedaAuto, SetValWaktu, alarmStatus);
    LCDSetValue(setValPHAuto, SetValTDSAuto, setvalJedaAuto, SetValWaktu);
  }
  else
  {
    ProgManual();
    setValueAuto = false;
    LCDSetValue(SetValPH, SetValTDS, SetValJeda, SetValWaktu);
  }


  
  LCDSuhu(DALLAS);
  LCDPhTds(PH, TDS);

  serialDisplay(MESINID, TEMP, HUM, PH, TDS, EC, DALLAS, SONAR1, SONAR2, level1, level2, level3, level4, level5, alarmStatus, FLOATSWITCH, FLOWMETER, AMPEREMETER);
}
