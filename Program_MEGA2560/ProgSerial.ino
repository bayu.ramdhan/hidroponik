unsigned long current_DispSerial = millis();
int serialStatus;
void serialDisplay(int mesinID, float dhtTemp, float dhtHum, float PH, float TDS, float EC, float dallas, 
                  float sonar1, float sonar2, float level1, float level2, float level3, float level4, float level5, 
                  int EMG, int floatswitch, float flowmeter, float amperemeter)
{
  unsigned long now_DispSerial = millis();
  if(abs(now_DispSerial - current_DispSerial) >= 10000){
    current_DispSerial = now_DispSerial;
    
    StaticJsonDocument<200> doc, doc2, doc3;
    doc["data"] = 1;
    doc["MESINID"] = mesinID;
    doc["DHTTEMP"] = dhtTemp;  
    doc["DHTHUM"] = dhtHum;
    doc["PH"] = PH;
    doc["TDS"] = TDS;
    doc["EC"] = EC;
    doc["DALLASTEMP"] = dallas;
    doc2["data"] = 2;
    doc2["SONAR1"] = sonar1;
    doc2["SONAR2"] = sonar2;
    doc2["EMG"] = EMG;
    doc2["FLOATSWITCH"] = floatswitch;
    doc2["LEVEL1"] = level1;
    doc2["LEVEL2"] = level2;
    doc2["LEVEL3"] = level3;
    doc3["data"] = 3;
    doc3["LEVEL4"] = level4;
    doc3["LEVEL5"] = level5;
    doc3["FLOWMETER"] = flowmeter;
    doc3["AMPEREMETER"] = amperemeter;
    
    if(serialStatus==0)
    {
      Serial.println("Kirim Data1");
      serializeJson(doc, Serial3);
      serialStatus = 1;
    }
    else if(serialStatus==1)
    {
      Serial.println("Kirim Data2");
      serializeJson(doc2, Serial3);
      serialStatus = 2;
    }
    else
    {
      Serial.println("Kirim Data3");
      serializeJson(doc3, Serial3);
      serialStatus = 0;
    }
    Serial.println();
    
    // serializeJsonPretty(doc, Serial3);
  }
}

String inString;
void serialEvent3() {
  while (Serial3.available()) {
    // Чтение данных из порта Serial3
    char inChar = Serial3.read();
    // Вывод прочитанных данных в порт Serial
    Serial.write(inChar);
    // Поиск команды в полученных данных (команда должна быть в квадратных скобках)
    inString += inChar;
    if (inChar == ']') {
      if(man_auto==true)
      {
        if (inString.indexOf("[ON]")>0) {
          setVal(pin_LampuTaman, HIGH);
          // Serial.println("LAMPU TAMAN ON");
  //        digitalWrite(PIN_LED, HIGH);
        }
        else if (inString.indexOf("[OFF]")>0) {
          setVal(pin_LampuTaman, LOW);
          // Serial.println("LAMPU TAMAN OFF");
  //        digitalWrite(PIN_LED, LOW);
        }
        else
        {
          Serial.println();
          Serial.print("Komunikasi dari ESP8266, text: ");
          // Serial.println(inString);
        }        
      }

      inString = "";
    }
  }
}
