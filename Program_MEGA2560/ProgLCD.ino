void LCDSuhu(float suhu)
{
  lcd.setCursor(0,0);
  lcd.print("SUHU:");
  lcd.setCursor(5,0);
  lcd.print(suhu);
  lcd.setCursor(9,0);                      // atur baris LCD baris 1 kolom ke 9
  lcd.print("\337C");
  lcd.setCursor(11,0);                      // atur baris LCD baris 1 kolom ke 0
  lcd.print("|");  
}

void LCDPhTds(float ph, float tds)
{
  lcd.setCursor(0, 1);  
  lcd.print("PH  :");                      // tampilkan tulisan "Temp:"
  lcd.setCursor(5, 1);                   // atur baris LCD baris 1 kolom ke 6
  lcd.print(ph,1);
  lcd.setCursor(10,1);                      // atur baris LCD baris 1 kolom ke 0
  lcd.print(" |");                      // tampilkan tulisan "Temp:"
  lcd.setCursor(0,2);                      // atur baris LCD baris 1 kolom ke 0
  lcd.print("TDS :");                      // tampilkan tulisan "Temp:"
  lcd.setCursor(5,2);                   // atur baris LCD baris 1 kolom ke 6
  lcd.print(tds);
  lcd.setCursor(10,2);                      // atur baris LCD baris 1 kolom ke 9
  lcd.print(" |");
  if(ph<10.00)
  {
    lcd.setCursor(9,1);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print(" ");
  }
  if(tds<10.00)
  {
    lcd.setCursor(6,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("    ");
  }
  else if(tds<100.00)
  {
    lcd.setCursor(7,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("   ");
  }
  else if(tds<1000)
  {
    lcd.setCursor(8,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("  ");
  }
  else
  {
    lcd.setCursor(9,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print(" ");
  }
}


void LCDSetValue(float SVph, float SVtds, int jeda, int waktu)
{
  lcd.setCursor(12,1);                   // atur baris LCD baris 1 kolom ke 6
  lcd.print(SVph,1); 
  lcd.setCursor(12,2);                   // atur baris LCD baris 1 kolom ke 6
  lcd.print(SVtds,0);
  lcd.setCursor(18,1);                  
  lcd.print(waktu); 
  lcd.setCursor(18,2);                  
  lcd.print(jeda); 
  
  if(SVph<10.00)
  {
    lcd.setCursor(15,1);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("   ");
  }
  else
  {
    lcd.setCursor(16,1);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("  ");
  }
  
  if(SVtds<100.00)
  {
    lcd.setCursor(14,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("    ");
  }
  else if(SVtds<1000.00)
  {
    lcd.setCursor(15,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("   ");
  }
  else
  {
    lcd.setCursor(16,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print("  ");
  }
  


  if(jeda<10)
  {
    lcd.setCursor(19,2);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print(" ");
  }

  if(waktu<10)
  {
    lcd.setCursor(19,1);                      // atur baris LCD baris 1 kolom ke 9
    lcd.print(" ");
  }
}

void LCDManual(bool nilai1, bool nilai2, bool nilai3, bool nilai4)
{
  if (nilai1 == HIGH ) { //ditekan
    lcd.setCursor(0,3);                      
    lcd.print("1 ON");
  } else {
    lcd.setCursor(0,3);                      
    lcd.print("1OFF");
  }
  lcd.setCursor(4,3);                      
  lcd.print(" ");
  
  if (nilai2 == HIGH) { //ditekan
     lcd.setCursor(5,3);                      
    lcd.print("2 ON");
  } else {
    lcd.setCursor(5,3);                      
    lcd.print("2OFF");
  }
  lcd.setCursor(9,3);                      
  lcd.print("  ");
  
  if (nilai3 == HIGH) { //ditekan
    lcd.setCursor(11,3);                      
    lcd.print("3 ON");
  } else {
    lcd.setCursor(11,3);                      
    lcd.print("3OFF");
  }
  lcd.setCursor(15,3);                      
  lcd.print(" ");
  
  if (nilai4 == HIGH) { //ditekan
    lcd.setCursor(16,3);                      
    lcd.print("4 ON");
  } else {
    lcd.setCursor(16,3);                      
    lcd.print("4OFF");
  } 
}

unsigned long current_DispPH = millis();
int DispPHSeconds = 0;
void LCDAutoPH(int statusPH, int jeda, int waktu)
{
  if(statusPH==0)
  {
    lcd.setCursor(0,3);                      
    lcd.print("PH OFF    ");  
  }
  else
  {
    String disp1 = "";
    String disp2 = "";
    if(statusPH==1)
    {
      disp1 = "PH TINGGI ";
      disp2 = " PH DW ON ";
    }
    else
    {
      disp1 = "PH RENDAH ";
      disp2 = " PH UP ON ";
    }
    unsigned long now_DispPH = millis();
    if(abs(now_DispPH - current_DispPH) >= 1000){
      current_DispPH = now_DispPH;
      DispPHSeconds++;
    }
    switch (DispPHSeconds) {
      case 0:
        lcd.setCursor(0,3);                      
        lcd.print(disp1);  
        break;
      case 1:
        lcd.setCursor(0,3);                      
        lcd.print(disp1);  
        break;
      case 3:
        lcd.setCursor(0,3);   
        lcd.print("          ");
        delay(10);     
        lcd.setCursor(0,3);  
        lcd.print(jeda);  
        lcd.setCursor(2,3);                      
        lcd.print("menit");
        break;
      case 5:
        lcd.setCursor(0,3);                      
        lcd.print(disp2);  
        break;
      case 7:
        lcd.setCursor(0,3);   
        lcd.print("          ");
        delay(10);              
        lcd.setCursor(0,3);  
        lcd.print(waktu);  
        lcd.setCursor(2,3);                      
        lcd.print("detik");
        break;
      case 9:
        DispPHSeconds = 0;
        break;
    }
  }
}

unsigned long current_DispTDS = millis();
int DispTDSSeconds = 0;
void LCDAutoTDS(int statusTDS, int jeda, int waktu)
{
  if(statusTDS==0)
  {
    lcd.setCursor(10,3);                      
    lcd.print("TDS OFF   ");  
  }
  else
  {
    String disp1 = "";
    String disp2 = "";
    if(statusTDS==1)
    {
      disp1 = "TDS TINGGI";
      disp2 = "AIRBAKU ON";
    }
    else
    {
      disp1 = "TDS RENDAH";
      disp2 = "AB MIX ON ";
    }
    unsigned long now_DispTDS = millis();
    if(abs(now_DispTDS - current_DispTDS) >= 1000){
      current_DispTDS = now_DispTDS;
      DispTDSSeconds++;
    }
    switch (DispTDSSeconds) {
      case 0:
        lcd.setCursor(10,3);                      
        lcd.print(disp1);  
        break;
      case 1:
        lcd.setCursor(10,3);                      
        lcd.print(disp1);  
        break;
      case 3:
        lcd.setCursor(10,3);                      
        lcd.print("          ");
        delay(10);             
        lcd.setCursor(10,3);                      
        lcd.print(jeda);  
        lcd.setCursor(12,3);                      
        lcd.print("menit");
        break;
      case 5:
        lcd.setCursor(10,3);                      
        lcd.print(disp2);  
        break;
      case 7:
        lcd.setCursor(10,3);   
        lcd.print("          ");
        delay(10);                   
        lcd.setCursor(10,3);                      
        lcd.print(waktu);  
        lcd.setCursor(12,3);                      
        lcd.print("detik");
        break;
      case 9:
        DispTDSSeconds = 0;
        break;
    }
  }
}
