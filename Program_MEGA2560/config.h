//MESIN ID
#define MESINID 1

//DIGITAL INPUT
#define pin_PBPHUp 7
#define pin_PBPHDown 6
#define pin_PBNutrisiA 5
#define pin_PBNutrisiB 4
#define pin_AUTO 8
#define pin_FloatSwitch 3
#define pin_NTP 46

//DIGITAL OUTPUT
#define pin_LEDHijau 40
#define pin_LEDMerah 41
//#define pin_Buzzer 41
#define pin_LampuTaman 36
#define pin_PompaAirAON 10
#define pin_PompaAirBaku 9
#define pin_PHUp 28
#define pin_PHDown 26
#define pin_NutrisiA 24
#define pin_NutrisiB 22
#define pin_AC 37 //undefined
#define pin_PompaAirLimit 52//undefined

//Digital Ultrasonic Water Level (SONAR)
#define pin_bak1Trig 16 //undefined
#define pin_bak1Echo 17 //undefined
#define pin_bak2Trig 33 //undefined
#define pin_bak2Echo 34 //undefined
#define bak1Min 0
#define bak1Max 20
#define bak2Min 0
#define bak2Max 20

//Digital DHT22
#define pin_DHT22 30
#define DHTMin 19
#define DHTMax 28

//ANALOG INPUT level sensor merah
#define pin_WaterLevel1 A8 //undefined
#define pin_WaterLevel2 A9 //undefined
#define pin_WaterLevel3 A10 //undefined
#define pin_WaterLevel4 A11 //undefined
#define pin_WaterLevel5 A12 //undefined
#define levelMin 0
#define levelMax 20
#define levelAlarm 15

//Analog Knob
#define pin_KnobPH A3
#define pin_KnobTDS A2
#define pin_KnobWaktu A4
#define pin_KnobJeda A5
float SetValPH = 7.00;
float SetValTDS = 800;
int SetValJeda = 1;
int SetValWaktu = 5;

//Analog TDS Sensor
#define pin_TDS A0


//Analog PH Sensor
#define pin_PH A1

//DIGITAL dallas sensor
#define pin_DallasTemp 2
//SetValue 

//ANALOG Flowmeter
#define pin_FlowMeter A13

//ANALOG Amperemeter
#define pin_Ampere A14 //pin for reading sensor
#define SENSOR_VCC_PIN A15 //pin for powring up the sensor
#define ZERO_CURRENT_LED_PIN 49 //zero current LED pin
//PIN48 dan PIN50 sudah terpakai untuk jumper power
