unsigned long current_autoPH = 0;
int PHSeconds = 0;
int PHStatus; //0 for normal, 1 for high, 2 for low
void AutoPH(float phVal, float phSet, int jedaSet, int waktuSet)
{
  unsigned long now_autoPH;

  int pin_ON, pin_OFF;
  if(phVal>phSet+0.5)
  {
    if (PHStatus!=1)
    {
      PHSeconds = 0;
      PHStatus = 1;
      current_autoPH = millis();
    }
    pin_ON = pin_PHDown;
    pin_OFF = pin_PHUp;
  }
  else if(phVal<phSet-0.5)
  {
    if (PHStatus!=2)
    {
      PHSeconds = 0;
      PHStatus = 2;
      current_autoPH = millis();
    }
    pin_ON = pin_PHUp;
    pin_OFF = pin_PHDown;
  }
  else
  {    
    setVal(pin_PHUp, LOW);
    setVal(pin_PHDown, LOW);
    PHStatus = 0;
  }
  if(PHStatus!=0)
  {
    setVal(pin_OFF, LOW);
    now_autoPH = millis();
    if(abs(now_autoPH - current_autoPH) >= 1000){
      current_autoPH = now_autoPH;
      PHSeconds++;
    }
    if(PHSeconds<=waktuSet)
    {
      setVal(pin_ON, HIGH);
    }
    else
    {
      setVal(pin_ON, LOW);
    }
    if(PHSeconds>=(jedaSet*60))
    {
      PHSeconds = 0;
    }
  }
  LCDAutoPH(PHStatus, jedaSet, waktuSet);
}

unsigned long current_autoTDS = 0;
int TDSSeconds = 0;
int TDSStatus; //0 for normal, 1 for high, 2 for low
void AutoTDS(float tdsVal, float tdsSet, int jedaSet, int waktuSet)
{
  unsigned long now_autoTDS;

  int pin_ON, pin_OFF;
  if(tdsVal>tdsSet+100)
  {
    if (TDSStatus!=1)
    {
      TDSSeconds = 0;
      TDSStatus = 1;
      current_autoTDS = millis();
    }
    pin_ON = pin_PompaAirLimit;
    pin_OFF = pin_NutrisiA;
    airBakuTDS = 1;
  }
  else if(tdsVal<tdsSet-100)
  {
    if (TDSStatus!=2)
    {
      TDSSeconds = 0;
      TDSStatus = 2;
      current_autoTDS = millis();
    }
    pin_ON = pin_NutrisiA;
    pin_OFF = pin_PompaAirLimit;
    airBakuTDS = 0;
  }
  else
  {    
    setVal(pin_NutrisiA, LOW);
    TDSStatus = 0;
    airBakuTDS = 0;
  }
  if(TDSStatus!=0)
  {
    setVal(pin_OFF, LOW);
    now_autoTDS = millis();
    if(abs(now_autoTDS - current_autoTDS) >= 1000){
      current_autoTDS = now_autoTDS;
      TDSSeconds++;
    }
    if(TDSSeconds<=waktuSet)
    {
      setVal(pin_ON, HIGH);
    }
    else
    {
      setVal(pin_ON, LOW);
    }
    if(TDSSeconds>=(jedaSet*60))
    {
      TDSSeconds = 0;
    }
  }
  LCDAutoTDS(TDSStatus, jedaSet, waktuSet);
  digitalWrite(pin_NutrisiB, digitalRead(pin_NutrisiA));
}
